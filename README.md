# Tareas - Redes de Computadoras

![Pipeline Status](https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes/badges/master/pipeline.svg)

En este repositorio estaremos manejando las tareas de la materia de [redes de computadoras][redes-gitlab] que se imparte en la [Facultad de Ciencias, UNAM][redes-fciencias] en el semestre 2021-2.

## Entregas

- Las entregas de las tareas y prácticas se realizan **en equipo** de `3` o `4` personas.
- **No se aceptan trabajos individuales**. Esto quedo definido en la [presentación del curso][redes-fciencias-presentacion].

## Organización de las entregas

Cada entrega debe cumplir con los siguientes lineamientos:

- Levantar un _merge request_ para entregar la actividad
    - El _flujo de trabajo_ viene documentado en [la carpeta `workflow`](./workflow)
- Cada **equipo** entregará sus tareas y prácticas dentro de la carpeta correspondiente.
- Cada tarea o práctica deberá tener lo siguiente:
    - Un archivo `README.md` donde se entregará la documentación en formato [_markdown_][guia-markdown].
    - Se puede hacer uso de un directorio `img` para guardar las imágenes o capturas de pantalla necesarias para la documentación
    - Una carpeta llamada `files` para incluir los archivos adicionales de la entrega en caso de ser necesario

### Estructura de directorios

La estructura de directorios que se manejará para las entregas es la siguiente:

- Una carpeta principal llamada `entregas`
    - Una carpeta por cada tarea (`entregas/tarea-01`) o práctica (`entregas/practica-02`)
        - Cada carpeta de tarea o práctica tendrá un archivo `README.md` donde se especifica lo que se deberá entregar
        - Cada equipo creará una carpeta llamada `Equipo-AAAA-BBBB-CCCC`, donde se realizará la entrega
            - `AAAA`, `BBBB` y `CCCC` son las iniciales de los integrantes del equipo

```
docs/
└── entrega/
    ├── README.md
    ├── tarea-0/
    │   ├── README.md
    │   ├── Equipo-AAAA-BBBB-CCCC/
    │   │   ├── img/
    │   │   │   └── ...
    │   │   ├── files/
    │   │   │   └── ...
    │   │   └── README.md
    │   └── Equipo-DDDD-EEEE-FFFF/
    │       └── ...
    ├── tarea-1/
    │   ├── README.md
    │   ├── Equipo-AAAA-BBBB-CCCC/
    │   │   └── ...
    │   └── Equipo-DDDD-EEEE-FFFF/
    │       └── ...
    ├── tarea-2/
    │   └── ...
    ├── practica-1/
    │   ├── README.md
    │   ├── Equipo-AAAA-BBBB-CCCC/
    │   │   └── ...
    │   └── Equipo-DDDD-EEEE-FFFF/
    │       └── ...
    ├── practica-2/
    │   └── ...
    └── ...
```

--------------------------------------------------------------------------------

- [Vista en GitLab](https://gitlab.com/Redes-Ciencias-UNAM/2021-2/tareas-redes)
- [Vista web](https://Redes-Ciencias-UNAM.gitlab.io/2021-2/tareas-redes)

--------------------------------------------------------------------------------

[redes-gitlab]: https://Redes-Ciencias-UNAM.gitlab.io/ "Página en GitLab"
[redes-fciencias]: http://www.fciencias.unam.mx/docencia/horarios/20212/1556/714 "Redes de Computadoras - 2021-2"
[redes-fciencias-presentacion]: http://www.fciencias.unam.mx/docencia/horarios/presentacion/322388 "Redes de Computadoras - 2021-2"
[guia-markdown]: https://about.gitlab.com/handbook/markdown-guide/
